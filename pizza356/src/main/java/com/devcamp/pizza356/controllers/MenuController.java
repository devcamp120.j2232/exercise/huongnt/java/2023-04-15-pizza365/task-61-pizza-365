package com.devcamp.pizza356.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza356.models.CMenu;
import com.devcamp.pizza356.services.MenuService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class MenuController {
    @Autowired MenuService menuService;
    @GetMapping("/combo-menu")
    public ResponseEntity<List<CMenu>> getAllComboAPI(){
        try {
			return new ResponseEntity<>(menuService.getAllMenu(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
}
