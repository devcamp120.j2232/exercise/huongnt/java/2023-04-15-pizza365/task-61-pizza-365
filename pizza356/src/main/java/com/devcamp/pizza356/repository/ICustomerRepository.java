package com.devcamp.pizza356.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza356.models.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long>{
    CCustomer findByCustomerId(long customerId);
    
}
