package com.devcamp.pizza356.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza356.models.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink,Long>{
    
}
