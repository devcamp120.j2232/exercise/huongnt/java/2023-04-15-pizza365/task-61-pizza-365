package com.devcamp.pizza356.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza356.models.CMenu;
import com.devcamp.pizza356.repository.IMenuRepository;

@Service
public class MenuService {
    @Autowired
    IMenuRepository pMenuRepository;
    public ArrayList<CMenu> getAllMenu() {
        ArrayList<CMenu> menuList = new ArrayList<>();
        pMenuRepository.findAll().forEach(menuList::add);
        return menuList;
    }
    
}
